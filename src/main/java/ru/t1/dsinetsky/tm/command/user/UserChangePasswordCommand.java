package ru.t1.dsinetsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_CHANGE_PASSWORD;

    @NotNull
    public static final String DESCRIPTION = "Change password of current user";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter new password:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final String id = getUserId();
        getUserService().changePassword(id, password);
        System.out.println("User successfully updated!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}
