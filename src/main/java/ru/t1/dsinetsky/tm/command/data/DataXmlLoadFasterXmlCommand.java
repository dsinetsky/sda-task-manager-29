package ru.t1.dsinetsky.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.Domain;
import ru.t1.dsinetsky.tm.exception.GeneralException;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_FASTERXML_XML_LOAD;

    @NotNull
    public static final String DESCRIPTION = "Loads data from xml file with FasterXML";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_XML));
        @NotNull final String data = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(data, Domain.class);
        setDomain(domain);
        System.out.println("File successfully loaded!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
