package ru.t1.dsinetsky.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.Domain;
import ru.t1.dsinetsky.tm.exception.GeneralException;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataBackupLoadJsonFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_FASTERXML_JSON_LOAD_BACKUP;

    @NotNull
    public static final String DESCRIPTION = "Loads data backup from json-backup file with FasterXML";

    @Override
    @SneakyThrows
    public void execute() throws GeneralException {
        @NotNull final byte[] bytes = Files.readAllBytes(Paths.get(FILE_BACKUP));
        @NotNull final String data = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new JsonMapper();
        @NotNull final Domain domain = objectMapper.readValue(data, Domain.class);
        setDomain(domain);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
