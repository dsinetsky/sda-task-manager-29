package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_PROJECT_COMPLETE_ID;

    @NotNull
    public static final String DESCRIPTION = "Completes project (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of project:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final Project project = getProjectService().changeStatusById(userId, id, Status.COMPLETED);
        showProject(project);
        System.out.println("Project successfully completed!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
