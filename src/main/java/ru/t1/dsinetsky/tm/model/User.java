package ru.t1.dsinetsky.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.enumerated.Role;

@Getter
@Setter
public final class User extends AbstractModel {

    @NotNull
    private String passwordHash;

    @NotNull
    private String login;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USUAL;

    private boolean locked = false;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Id: ").append(getId()).append("\n")
                .append("Login: ").append(login).append(" - ").append(role.getDisplayName()).append("\n")
                .append("Name: ").append(lastName).append(" ").append(firstName).append(" ").append(middleName).append("\n")
                .append("Email: ").append(email).toString();
    }

}
