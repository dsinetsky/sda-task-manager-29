package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.api.service.ITestCreateService;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.User;

public class TestCreateService implements ITestCreateService {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    public TestCreateService(@NotNull final IProjectService projectService, @NotNull final ITaskService taskService, @NotNull final IUserService userService) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void createTest() throws GeneralException {
        userService.createTest();
        for (@NotNull final User user : userService.returnAll()) {
            projectService.createTest(user);
            taskService.createTest(user);
        }
    }

}
