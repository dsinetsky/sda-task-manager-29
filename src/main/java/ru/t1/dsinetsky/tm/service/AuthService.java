package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.service.IAuthService;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.builder.repository.UserBuilder;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.*;
import ru.t1.dsinetsky.tm.model.User;
import ru.t1.dsinetsky.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Objects;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService, @NotNull final IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    public User registry(@NotNull final String login, @NotNull final String password) throws GeneralException {
        return userService.add(UserBuilder.create().login(login).password(password).toUser());
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordIsEmptyException();
        if (!userService.isUserExistByLogin(login)) throw new IncorrectLoginPasswordException();
        @NotNull final User user = userService.findUserByLogin(login);
        if (user.isLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!Objects.equals(hash, user.getPasswordHash())) throw new IncorrectLoginPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    @NotNull
    public String getUserId() throws GeneralUserException {
        if (!isAuth()) throw new UserNotLoggedException();
        return userId;
    }

    @Override
    @NotNull
    public User getUser() throws GeneralException {
        if (!isAuth()) throw new UserNotLoggedException();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new UserNotLoggedException();
        return user;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) throws GeneralException {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        //if (role == null) throw new AccessDeniedException();
        final boolean hasAccess = Arrays.asList(roles).contains(role);
        if (!hasAccess) throw new AccessDeniedException();
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws GeneralException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!userService.isUserExistByLogin(login)) throw new UserNotFoundException();
        @NotNull final User user = userService.findUserByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws GeneralException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!userService.isUserExistByLogin(login)) throw new UserNotFoundException();
        @NotNull final User user = userService.findUserByLogin(login);
        user.setLocked(false);
    }

}
