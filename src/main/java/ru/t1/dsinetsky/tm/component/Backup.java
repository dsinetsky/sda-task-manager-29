package ru.t1.dsinetsky.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.command.data.AbstractDataCommand;
import ru.t1.dsinetsky.tm.command.data.DataBackupLoadJsonFasterXmlCommand;
import ru.t1.dsinetsky.tm.command.data.DataBackupSaveJsonFasterXmlCommand;
import ru.t1.dsinetsky.tm.exception.GeneralException;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class Backup extends Thread {

    @NotNull
    final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        this.setDaemon(true);
    }

    public void init() throws GeneralException {
        load();
        start();
    }

    public void save() throws GeneralException {
        bootstrap.terminalRun(DataBackupSaveJsonFasterXmlCommand.NAME, false);
    }

    public void load() throws GeneralException {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.terminalRun(DataBackupLoadJsonFasterXmlCommand.NAME, false);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            Thread.sleep(30000);
            save();
        }
    }

}
